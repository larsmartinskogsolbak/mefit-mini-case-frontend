﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MeFit_API.Models;

namespace MeFit_API.Data
{
    public class MeFit_APIContext : DbContext
    {
        public MeFit_APIContext(DbContextOptions<MeFit_APIContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Models.Program_> Programs { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkouts { get; set;}

        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<GoalWorkout> GoalWorkouts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProgramWorkout>().HasKey(pw => new { pw.ProgramId, pw.WorkoutId });
            modelBuilder.Entity<GoalWorkout>().HasKey(gw => new { gw.GoalId, gw.WorkoutId });
        }

    }
}
