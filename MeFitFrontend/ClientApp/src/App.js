import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import { FetchData } from "./components/FetchData";
import { Counter } from "./components/Counter";
import "./custom.css";
import { CreateProfile } from "./components/CreateProfile";
import { ViewProfiles } from "./components/ViewProfiles";
import { Exercises } from "./components/Exercises";
import { ViewExercises } from "./components/ViewExercises";
import { CreateProgram } from "./components/CreateProgram";
import { ViewPrograms } from "./components/ViewPrograms";

export default class App extends Component {
  static displayName = App.name;

  render() {
    return (
      <Layout>
        <Route exact path="/" component={Home} />
        <Route path="/counter" component={Counter} />
        <Route path="/fetch-data" component={FetchData} />
        <Route path="/create-profile" component={CreateProfile} />
        <Route path="/view-profiles" component={ViewProfiles} />
        <Route path="/exercises" component={Exercises} />
        <Route path="/view-exercises" component={ViewExercises} />
        <Route path="/create-program" component={CreateProgram} />
        <Route path="/view-programs" component={ViewPrograms} />
      </Layout>
    );
  }
}
