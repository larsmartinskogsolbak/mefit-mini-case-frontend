import React, { Component } from "react";
import Table from "react-bootstrap/Table";


export class ViewProfiles extends React.Component{
    static displayName = ViewProfiles.name;
    state = {
        profiles: []
    }
    async componentDidMount(){
        const url = "https://localhost:5001/api/";
        const response = await fetch(url + "profiles");
        const json = await response.json();
        await this.setState({
            profiles: json ,
        });
    }

    render(){
        console.log(this.state.profiles)
        if(this.state.profiles != null){
            return(
                <div>
              <h1>View Profiles</h1>
              <br/>
              <div id="profiles">
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Weight</th>
                      <th>Height</th>
                      <th>Medical Condition</th>
                      <th>Address Line 1</th>
                      <th>Address Line 2</th>
                      <th>Address Line 3</th>
                      <th>Postal Code</th>
                      <th>City</th>
                      <th>Country</th>

                    </tr>
                  </thead>
                  <tbody>
               {this.state.profiles.map((profile, i) =>
               <tr>
                <td>{profile.id}</td>
                <td>{profile.user.firstName}</td>
                <td>{profile.user.lastName}</td>
                <td>{profile.weight}</td>
                <td>{profile.height}</td>
                <td>{profile.medicalCondition}</td>
                <td>{profile.address.addressLine1}</td>
                <td>{profile.address.addressLine2}</td>
                <td>{profile.address.addressLine3}</td>
                <td>{profile.address.postalCode}</td>
                <td>{profile.address.city}</td>
                <td>{profile.address.country}</td>

               </tr>
               )}                  
                  </tbody>
                </Table>
              </div></div>
            )
        }
        else return(<div>loading...</div>)
    }
}