import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";

export class CreateProfile extends Component {
  static displayName = CreateProfile.name;
  state = {
    profile: {}
  };

  /*  async componentDidMount() {
    const url =
      "https://localhost:5001/api/profile/" + this.props.match.params.id;

    const response = await fetch(url).then(resp => resp.json());
    let pro = response;
    this.setState({
      profile: pro
    });
    console.log(pro);
  } */
  async postProfile() {
    console.log(this.state.profile)
    const url = "https://localhost:5001/api/profiles";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.profile)
    });
    await response.json();
    await setTimeout(3000);
    this.props.history.push('/view-profiles')
    //console.log(response);
  }

  async CreateProfile(e) {
    e.preventDefault();
    await this.setState({
      profile: {
        weight: parseInt(this.inputWeight.value),
        height: parseInt(this.inputHeight.value),
        medicalCondition: this.inputMedCon.value,
        disability: this.inputDis.value,
        user: {
          firstName: this.inputFirstName.value,
          lastName: this.inputLastName.value
        },
        address: {
          addressLine1: this.inputAddressLine1.value,
          addressLine2: this.inputAddressLine2.value,
          addressLine3: this.inputAddressLine3.value,
          postalCode: parseInt(this.inputPost.value),
          city: this.inputCity.value,
          country: this.InputCountry.value
        } 
      }
    })
    console.log(this.inputAddressLine2.value);
    this.postProfile();
  }

  /* async putProfile() {
      console.log(this.props.match.params.id)
    const url =
      "https://localhost:5001/api/profile/" + this.props.match.params.id;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.profile)
    });
    console.log(response);
  } */

  render() {
    return (
      <div>
        <h1>Create a Profile</h1>

        <div>
          <Form onSubmit={this.CreateProfile.bind(this)}>
            
            {/* <Form.Group controlId="formBasicEmail">
   <Form.Label>Email address</Form.Label>
   <Form.Control type="email" placeholder="Enter email" />
   </Form.Group> */}
            <br />
            <Form.Group>
              <Form.Control
                ref={input => (this.inputFirstName = input)}
                type="text"
                placeholder="First Name"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputLastName = input)}
                type="text"
                placeholder="Last Name"
              />
            </Form.Group>
            <br />
            <Form.Group>
              <Form.Control
                ref={input => (this.inputWeight = input)}
                type="number"
                placeholder="Weight (kg)"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputHeight = input)}
                type="number"
                placeholder="Height (cm)"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputMedCon = input)}
                type="text"
                placeholder="Medical Condition"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputDis = input)}
                type="text"
                placeholder="Disability"
              />
            </Form.Group>
            <br />
            <Form.Group>
              <Form.Control
                ref={input => (this.inputAddressLine1 = input)}
                type="text"
                placeholder="Address Line 1"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputAddressLine2 = input)}
                type="text"
                placeholder="Address Line 2"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputAddressLine3 = input)}
                type="text"
                placeholder="Address Line 3"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputPost = input)}
                type="number"
                placeholder="Postal Code"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputCity = input)}
                type="text"
                placeholder="City"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.InputCountry = input)}
                type="text"
                placeholder="Country"
              />
            </Form.Group>
            <br />
            {/* <Form.Group controlId="formBasicPassword">
   <Form.Label>Password</Form.Label>
   <Form.Control type="password" placeholder="Password" />
 </Form.Group> */}
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}
