import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

export class ViewExercises extends Component {
  static displayName = ViewExercises.name;
  state = {
    exercises: null
  };

  async componentDidMount() {
    const url = "https://localhost:5001/api/exercises";
    const response = await fetch(url);
    const json = await response.json();
    //console.log(json);
    await this.setState({
      exercises: json,
      exercise: {}
    });
    console.log(this.state.exercises);
  }
  async viewExercise(exercise) {
    this.inputId.value = exercise.exerciseId;
    this.inputName.value = exercise.name;
    this.inputDesc.value = exercise.description;
    this.inputTarget.value = exercise.targetDescriptionGroup;
    this.inputImg.value = exercise.image;
    this.inputVid.value = exercise.vidLink;
  }
  

  async updateExercise(e) {
    e.preventDefault();
    await this.setState({
      exercise: {
        exerciseId: parseInt(this.inputId.value),
        name: this.inputName.value,
        description: this.inputDesc.value,
        targetDescriptionGroup: this.inputTarget.value,
        image: this.inputImg.value,
        vidLink: this.inputVid.value
      }
    });
    this.putExercise();
  }

  async putExercise() {
    const url = "https://localhost:5001/api/exercises/" + this.inputId.value;
    await fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.exercise)
    });
    window.location.reload();

  }

  async deleteExercise(){
    const url = "https://localhost:5001/api/exercises/" + this.inputId.value;
  await fetch(url, {
      method: 'DELETE'
  });
// const json = await response.json();
  window.location.reload();

  }

  render() {
    //console.log(this.state.exercises);
    if (this.state.exercises != null) {
      return (
        <div>
          <h1>All Exercises</h1>
          <h4>Click an exercise to update/delete</h4>
          <br />
          <div id="exercises">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Exercise Name</th>
                  <th>Description</th>
                  <th>Target muscle group</th>
                  <th>Image Link</th>
                  <th>Video Link</th>
                </tr>
              </thead>
              <tbody>
                {this.state.exercises.map((exercise, i) => (
                  <tr key={i} onClick={() => this.viewExercise(exercise)}>
                    <td>{exercise.exerciseId}</td>
                    <td>{exercise.name}</td>
                    <td>{exercise.description}</td>
                    <td>{exercise.targetDescriptionGroup}</td>
                    <td>{exercise.image}</td>
                    <td>{exercise.vidLink}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
          <h2>Update/delete an Exercise</h2>
          <br />
          <Form onSubmit={this.updateExercise.bind(this)}>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputId = input)}
                type="number"
                placeholder="Exercise ID"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputName = input)}
                type="text"
                placeholder="Exercise Name"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputDesc = input)}
                type="text"
                placeholder="Exercise description"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputTarget = input)}
                type="text"
                placeholder="Target muscle group"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputImg = input)}
                type="text"
                placeholder="Image link"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputVid = input)}
                type="text"
                placeholder="Video link"
              />
            </Form.Group>
            <br />
            <Button variant="primary" type="submit">
              Update
            </Button><span>&nbsp;&nbsp;</span>
            <Button variant="danger" onClick={this.deleteExercise.bind(this)}>
              Delete
            </Button>
          </Form>
        </div>
      );
    } else return <div>loading...</div>;
  }
}
