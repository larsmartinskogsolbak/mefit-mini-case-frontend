import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export class CreateProgram extends Component {
  static displayName = CreateProgram.name;
  state = {
    program: {},
    programs: null
  };


  async createProgram(e) {
    e.preventDefault()
    await this.setState({
      program: {
        name: this.inputName.value,
        category: this.inputCategory.value
      }
    })
    this.postProgram();
  }

  async postProgram(){
    console.log(this.state.program)
    const url = "https://localhost:5001/api/program_"
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.program)
    });
    await response.json();
    await setTimeout(3000);
    this.props.history.push('/view-programs')

  }

  render() {
    return (
      <div>
        <h1>Create a Program</h1>
        <br/> 
        <div>
          <Form onSubmit={this.createProgram.bind(this)}>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputName = input)}
                type="text"
                placeholder="Program name"
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                ref={input => (this.inputCategory = input)}
                type="text"
                placeholder="Program category"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Create program
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}
