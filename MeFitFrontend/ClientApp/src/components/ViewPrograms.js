import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

export class ViewPrograms extends Component {
  static displayName = ViewPrograms.name;
  state = {
    programs: null
  };

  async componentDidMount(){
    const url = "https://localhost:5001/api/program_"
    const response = await fetch(url);
    const json = await response.json();
    console.log(json);
    await this.setState({
      programs: json,
      program: {}
    })
  }

  render(){
      if(this.state.programs != null){
      return(
          <div>
              <h1>All programs</h1>
              <br/>
              <div>
              <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Program name</th>
                  <th>Program category</th>
                </tr>
              </thead>
              <tbody>
                {this.state.programs.map((program, i) => (
                  <tr>
                    <td>{program.id}</td>
                    <td>{program.name}</td>
                    <td>{program.category}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
              </div>
          </div>
      )
                 } else return <div>loading...</div>
      }
  }
