import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

export class Exercises extends Component {
  static displayName = Exercises.name;
  state = {
    exercise: {}
  };

  async postExercise() {
    console.log(this.state.exercise);
    const url = "https://localhost:5001/api/exercises";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state.exercise)
    });
    await response.json();
    this.props.history.push('/view-exercises')
  }

  async createExercise(e) {
    e.preventDefault();
    await this.setState({
      exercise: {
        name: this.inputName.value,
        description: this.inputDesc.value,
        targetDescriptionGroup: this.inputTarget.value,
        image: this.inputImg.value,
        vidLink: this.inputVid.value
      }
    });
    console.log(this.inputName.value);
    this.postExercise();
   
  }
  render() {
    return (
      <div>
        <h1>Create an Exercise</h1>
        <br />
        <Form onSubmit={this.createExercise.bind(this)}>
          <Form.Group>
            <Form.Control
              ref={input => (this.inputName = input)}
              type="text"
              placeholder="Exercise Name"
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              ref={input => (this.inputDesc = input)}
              type="text"
              placeholder="Exercise description"
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              ref={input => (this.inputTarget = input)}
              type="text"
              placeholder="Target muscle group"
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              ref={input => (this.inputImg = input)}
              type="text"
              placeholder="Image link"
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              ref={input => (this.inputVid = input)}
              type="text"
              placeholder="Video link"
            />
          </Form.Group>
          <br />
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
        <div>
          <br />
        </div>
      </div>
    );
  }
}
