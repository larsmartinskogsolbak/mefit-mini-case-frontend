﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Program_
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public virtual Profile Profile { get; set; }
        public List<ProgramWorkout> ProgramWorkouts { get; set; }

        public Program_()
        {
            ProgramWorkouts = new List<ProgramWorkout>();

        }
    }
}
