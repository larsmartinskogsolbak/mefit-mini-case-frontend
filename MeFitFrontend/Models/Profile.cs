﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public int Weight { get; set; } = 0;
        public int Height { get; set; } = 0;
        public string MedicalCondition { get; set; } = "No";
        public string Disability { get; set; } = "No";
        public User User { get; set; }
        public virtual Address Address { get; set; }
        [ForeignKey("Program")]
        public int? ProgramId { get; set; }
        public virtual Program_ Program { get; set; }
        [ForeignKey("Set")]
        public int? SetId { get; set; }
        public virtual Set Set { get; set; }
        [ForeignKey("Goal")]
        public int? GoalId { get; set; }
        public virtual Goal Goal { get; set; }
        public Profile() { }

    }
}
