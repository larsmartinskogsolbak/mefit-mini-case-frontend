﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Goal
    {
        public int GoalId { get; set; }
        public string EndData { get; set; }
        public bool Achieved { get; set; }
        [ForeignKey("Program")]
        public int ProgramId { get; set; }
        public virtual Profile Profile { get; set; }
        public List<GoalWorkout> GoalWorkouts { get; set; }

        public Goal()
        {
            GoalWorkouts = new List<GoalWorkout>();
        }

    }
}
