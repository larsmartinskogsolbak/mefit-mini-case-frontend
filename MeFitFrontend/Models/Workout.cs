﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Workout
    {
        public int WorkoutId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
        public List<ProgramWorkout> ProgramWorkouts { get; set; }
        public List<GoalWorkout> GoalWorkouts { get; set; }
        public Workout()
        {
            ProgramWorkouts = new List<ProgramWorkout>();
            GoalWorkouts = new List<GoalWorkout>();
        }

        [ForeignKey("Set")]
        public int SetId { get; set; }

        public virtual Set Set { get; set; }
        
    }
}
