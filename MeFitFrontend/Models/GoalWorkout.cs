﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class GoalWorkout
    {
        public GoalWorkout()
        {

        }
        public GoalWorkout(Goal goal, Workout workout)
        {
            Goal = goal;
            Workout = workout;
        }
        [Key, Column(Order = 1)]
        public int GoalId { get; set; }
        [Key, Column(Order = 2)]
        
        public int WorkoutId { get; set; }
        public Goal Goal { get; set; }
        public Workout Workout { get; set; }
    }
}
