﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class ProgramWorkout
    {
        public ProgramWorkout()
        {

        }
        public ProgramWorkout(Program_ program, Workout workout)
        {
            Program = program;
            Workout = workout;
        }
        [Key, Column(Order = 1)]
        public int ProgramId { get; set; }
        [Key, Column(Order = 2)]
        public int WorkoutId { get; set; }

        public Program_ Program { get; set; }
        public Workout Workout { get; set; }
    }
}
