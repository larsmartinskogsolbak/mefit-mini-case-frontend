﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Set
    {
        public int SetId { get; set; }
        public int ExerciseRepetitions { get; set; }
        [ForeignKey("Exercise")]
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual Workout Workout { get; set; }
    }
}
