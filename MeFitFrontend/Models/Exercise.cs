﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFit_API.Models
{
    public class Exercise
    {
        public int ExerciseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TargetDescriptionGroup { get; set; }
        public string Image { get; set; }
        public string VidLink { get; set; }
        
        public List<Set> Sets { get; set; }
        
    }
}
