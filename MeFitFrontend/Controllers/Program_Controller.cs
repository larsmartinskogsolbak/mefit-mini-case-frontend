﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MeFit_API.Data;
using MeFit_API.Models;

namespace MeFitFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Program_Controller : ControllerBase
    {
        private readonly MeFit_APIContext _context;

        public Program_Controller(MeFit_APIContext context)
        {
            _context = context;
        }

        // GET: api/Program_
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Program_>>> GetPrograms()
        {
            return await _context.Programs.ToListAsync();
        }

        // GET: api/Program_/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Program_>> GetProgram_(int id)
        {
            var program_ = await _context.Programs.FindAsync(id);

            if (program_ == null)
            {
                return NotFound();
            }

            return program_;
        }

        // PUT: api/Program_/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProgram_(int id, Program_ program_)
        {
            if (id != program_.Id)
            {
                return BadRequest();
            }

            _context.Entry(program_).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Program_Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Program_
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Program_>> PostProgram_(Program_ program_)
        {
            _context.Programs.Add(program_);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProgram_", new { id = program_.Id }, program_);
        }

        // DELETE: api/Program_/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Program_>> DeleteProgram_(int id)
        {
            var program_ = await _context.Programs.FindAsync(id);
            if (program_ == null)
            {
                return NotFound();
            }

            _context.Programs.Remove(program_);
            await _context.SaveChangesAsync();

            return program_;
        }

        private bool Program_Exists(int id)
        {
            return _context.Programs.Any(e => e.Id == id);
        }
    }
}
