Extends the MeFit API to contain a React frontend application. 

The application contains a home page with separate pages for creating and viewing user profiles and programs.

It also contains pages for creating, viewing, updating and deleting programs. 


```
git clone git@gitlab.com:larsmartinskogsolbak/mefit-mini-case-frontend.git

cd mefit-mini-case-frontend/MeFitFrontend

dotnet run
```

